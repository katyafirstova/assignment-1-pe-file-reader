/// @file
/// @brief Definitions of functions for work with PE file

#ifndef PE_H_
#define PE_H_

#include "pe_structure.h"


/// @brief Read PE file
/// @param[in] in File to read
/// @param[in] pe_file Structure containing PE file data
/// @return  true in case of success
bool read(FILE* in, struct pe_file* pe_file);

/// @brief Write section to file
/// @param[in] in Input file to read
/// @param[in] out Output file to write
/// @param[in] sectionHeader Section to write
/// @return true in case of success
bool write_section(FILE* in, FILE* out, const struct pe_section_header* sectionHeader);

/// @brief Extract section by name and write to file
/// @param[in] in Input file to read
/// @param[in] out Output file to write section
/// @param[in] section_name  Name of the section
/// @return true in case of success
bool extract_section(FILE* const in, FILE* const out, const char* section_name);
/// @brief Get section by name
/// @param[in] section_name Name of section
/// @param[in] pe_file Structure containing PE file data
/// @return  founded section
struct pe_section_header* get_section(const char* section_name, const struct pe_file* file);

#endif
