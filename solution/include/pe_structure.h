/// @brief Definition of PE File structure

#ifndef PE_FILE_H
#define PE_FILE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// Definition of types
typedef uint32_t DWORD;
typedef uint16_t WORD;

/// Size of name of section header
#define NAME_SIZE 8

#ifdef _MSK_VER
#pragma pack(push, 1)
#endif

/// @brief Standard Common Object File Format file header
struct 
#ifdef __GNUC__
        __attribute__((packed))
#endif
pe_header {

  /// Identifies the type of target machine
  WORD  machine;
  /// Indicates the size of the section table
  WORD  numberOfSections;
  /// Indicates when the file was created. 
  DWORD timeDateStamp;
  /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
  DWORD pointerToSymbolTable;
  /// The number of entries in the symbol table
  DWORD numberOfSymbols;
  /// The size of the optional header(only for executable files)
  WORD  sizeOfOptionalHeader;
  /// The flags that indicate the attributes of the file
  WORD  characteristics;

};

/// @brief Section Headers
struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
pe_section_header {
    /// An 8-byte, null-padded UTF-8 encoded string.
    uint8_t name[NAME_SIZE];
    /// The total size of the section when loaded into memory
    DWORD virtualSize;
    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
    DWORD virtualAddress;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    DWORD SizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file.
    DWORD pointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.
    DWORD pointerToRelocations;
    ///The file pointer to the beginning of line-number entries for the section. This is set to zero if there are no COFF line numbers.
    DWORD pointerToLineNumbers;
    /// The number of relocation entries for the section. This is set to zero for executable images.
    WORD numberOfRelocations;
    /// The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated.
    WORD numberOfLineNumbers;
    /// The flags that describe the characteristics of the section.
    DWORD characteristics;
};

/// @brief PE File structure
struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
pe_file {

    /// identifies the file as a PE file
    uint32_t signature;

    /// Offset to a file signature
    uint32_t header_offset;

    /// COFF File Header
    struct pe_header header;

    /// Section Headers
    struct pe_section_header *section_headers;

};

#ifdef _MSK_VER
#pragma pack(pop)
#endif


#endif

