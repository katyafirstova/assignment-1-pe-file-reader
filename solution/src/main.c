#include "pe.h"
#include "pe_structure.h"

/// @file 
/// @brief Main application file

#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv) {

if (argc < 4) {
   usage(stdout);
   return 1;
} 
        
FILE *in = fopen(argv[1], "rb");
if (!in) {
    fprintf(stderr, "Error: failed to open input file");
    fclose(in);
    return 1;
}
        
FILE *out = fopen(argv[3], "wb");
if (!out) {
    fprintf(stderr, "Error: failed to open output file");
    fclose(in);
    fclose(out);         
    return 1;
}
        
bool result = extract_section(in, out, argv[2]);
if (fclose(in) != 0) fprintf(stderr, "Error: failed to close input file");
if (fclose(out) != 0) fprintf(stderr, "Error: failed to close output file");
        
return !result;
    
}




















