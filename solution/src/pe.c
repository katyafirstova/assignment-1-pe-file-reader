/// @brief Functions to work with PE File

#include "pe.h"
#include "pe_structure.h"

/// @brief Offset to the PE signature
#define OFFSET 0x3c

/// @brief Read PE file
/// @param[in] in File to read
/// @param[in] pe_file Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
bool read(FILE* const in, struct pe_file* const pe_file) {
    if (in) {
        if (fseek(in, OFFSET, SEEK_SET) != 0) {
            fprintf(stderr, "Error: can't move the pointer");
            return false;
        }
        if (!fread(&pe_file->header_offset, sizeof(pe_file->header_offset), 1, in)) {
            fprintf(stderr, "Error: can't read header offset");
            return false;
        }
        if (fseek(in, pe_file->header_offset, SEEK_SET) != 0) {
            fprintf(stderr, "Error: can't move the pointer");
            return false;
        }
        if (!fread(&pe_file->signature, sizeof(pe_file->signature), 1, in)) {
            fprintf(stderr, "Error: can't read signature");
            return false;
        }
        if (!fread(&pe_file->header, sizeof(struct pe_header), 1, in)) {
            fprintf(stderr, "Error: can't read header");
            return false;
        }

        if (fseek(in, pe_file->header.sizeOfOptionalHeader, SEEK_CUR) != 0) {
            fprintf(stderr, "Error: can't move the pointer");
            return false;
        }

        pe_file->section_headers = malloc(pe_file->header.numberOfSections * sizeof(struct pe_section_header));

        for (uint16_t i = 0; i < pe_file->header.numberOfSections; i++) {
            if (!fread(&pe_file->section_headers[i], sizeof(struct pe_section_header), 1, in)) {
                fprintf(stderr, "Error: can't read the section header");
                return false;
            }
        }
        return true;
    }
    fprintf(stderr, "Error: the file pointer is null");
    return false;

}


/// @brief Get section by name
/// @param[in] section_name Name of section
/// @param[in] pe_file Structure containing PE file data
/// @return  founded section
struct pe_section_header* get_section(const char* const section_name, const struct pe_file* const file) {
    for (uint16_t i = 0; i < file->header.numberOfSections; i++) {
        if (memcmp(file->section_headers[i].name, section_name, strlen(section_name)) == 0) {
            return file->section_headers + i;
        }
    }
    return NULL;
}

/// @brief Write section to file
/// @param[in] in Input file to read
/// @param[in] out Output file to write
/// @param[in] sectionHeader Section to write
/// @return true in case of success
bool write_section(FILE* const in, FILE* const out, const struct pe_section_header* const sectionHeader) {
    if (sectionHeader) {
        if (fseek(in, sectionHeader->pointerToRawData, SEEK_SET) != 0) {
            fprintf(stderr, "Error: can't move the pointer");
            return false;
        }
        char data[sectionHeader->SizeOfRawData];
        if (!fread(data, sizeof(data), 1, in)) {
            fprintf(stderr, "Error: can't read the section data");
            return false;
        }
        if (!fwrite(data, sizeof(data), 1, out)) {
            fprintf(stderr, "Error: can't write the section data");
            return false;
        }
        return true;
    }
    fprintf(stderr, "Error: can't write the section data. Section header is null");
    return false;
}

/// @brief Extract section by name and write to file
/// @param[in] in Input file to read
/// @param[in] out Output file to write section
/// @param[in] section_name  Name of the section
/// @return true in case of success
bool extract_section(FILE* const in, FILE* const out, const char* section_name) {
    struct pe_file PE_file = {0};
    if (read(in, &PE_file)) {
        bool res = write_section(in, out, get_section(section_name, &PE_file));
        free(PE_file.section_headers);
        return res;
    }
    free(PE_file.section_headers);
    return false;
}
